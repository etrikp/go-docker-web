/*global  angular:false */
/*jslint sub: true, browser: true, indent: 4, vars: true, nomen: true */
'use strict';

// Declare app level module which depends on filters, and services
var app = angular.module('godweb', ['god.resources', 'ngSanitize', 'ngCookies', 'ngRoute', 'ui.utils', 'ui.bootstrap', 'datatables', 'ui.codemirror', 'angular-growl', 'btford.socket-io', 'chart.js'])
.config(function($routeProvider, $locationProvider) {
  $routeProvider
  .when('/', {
    templateUrl: 'views/welcome.html',
    controller: 'welcomeCtrl'
  })
  .when('/usage', {
    templateUrl: 'views/usage.html',
    controller: 'usageCtrl'
  })
  .when('/user/:user', {
    templateUrl: 'views/user.html',
    controller: 'userInfoCtrl'
  })
  .when('/user/:user/files', {
    templateUrl: 'views/jobfiles.html',
    controller: 'userFilesCtrl'
  })
  .when('/projects', {
    templateUrl: 'views/projects.html',
    controller: 'projectsCtrl'
  })
  .when('/images', {
    templateUrl: 'views/images.html',
    controller: 'imagesCtrl'
  })
  .when('/jobs', {
    templateUrl: 'views/jobs.html',
    controller: 'jobsCtrl'
  })
  .when('/admin', {
    templateUrl: 'views/admin.html',
    controller: 'adminCtrl'
  })
  .when('/job', {
    templateUrl: 'views/job.html',
    controller: 'jobCtrl'
  })
  .when('/job/:jobid/live', {
    templateUrl: 'views/joblive.html',
    controller: 'jobLiveCtrl'
  })
  .when('/job/replay/:id', {
    templateUrl: 'views/job.html',
    controller: 'jobCtrl'
  })
  .when('/job/:jobid/files', {
    templateUrl: 'views/jobfiles.html',
    controller: 'jobFilesCtrl'
  })
  .when('/job/:jobid/monitor', {
      templateUrl: 'views/jobmonitor.html',
      controller: 'jobMonitorCtrl'
  })
  .when('/marketplace/recipe', {
      templateUrl: 'views/recipes.html',
      controller: 'recipesCtrl'
  })
  .when('/marketplace/recipe/:id', {
      templateUrl: 'views/recipe.html',
      controller: 'recipeCtrl'
  })
  .when('/marketplace/task/:id/link', {
      templateUrl: 'views/recipe_link.html',
      controller: 'recipeLinkCtrl'
  })
  .when('/login', {
    templateUrl: 'views/login.html',
    controller: 'loginCtrl'
  });

})
.config(['growlProvider', function(growlProvider) {
    growlProvider.globalTimeToLive(5000);
}])
.config(['$httpProvider', function ($httpProvider){
    $httpProvider.interceptors.push( function($q, $window){
        return {
        'request': function (config) {
                config.headers = config.headers || {};
                if ($window.sessionStorage.token) {
                    config.headers.Authorization = 'Bearer ' + $window.sessionStorage.token;
                }
                return config;
            },
            'response': function(response){
                return response;
            },
            'responseError': function(rejection){
                if(rejection.status == 401) {
                    // Route to #/login
                    location.replace('#/login');
                }
                return $q.reject(rejection);
            }
        };
    });
}])
.controller('welcomeCtrl',
    function ($scope, $route, Stat, growl, Admin) {
        $scope.total = 0;
        Stat.count_status({'status': 'pending'}).$promise.then(function(data) {
            $scope.pending = data['total'];
        });
         Stat.count_status({'status': 'running'}).$promise.then(function(data) {
             $scope.running = data['total'];
         });
         Stat.count_status({'status': 'all'}).$promise.then(function(data) {
             $scope.total = data['total'];
         });

         $scope.maintenance = false;
         Admin.maintenance().$promise.then(function(data){
             if(data.status.general == 'on') {
                 $scope.maintenance = true;
             }
             else {
                 $scope.maintenance = false;
             }
         });
})
.controller('usageCtrl',
    function ($scope, $route, GoDUsage) {
        $scope.usage = [];
        GoDUsage.get().$promise.then(function(usage) {
            $scope.usage = usage;
        });
})
.controller('recipeLinkCtrl',
    function ($scope, $route, $routeParams, $location, Recipes, Auth) {
        $scope.user = Auth.getUser();

        $scope.list_my_recipes = function() {
            Recipes.query({'my': 1}).$promise.then(function(data) {
                $scope.recipes = data;
            });
        };

        $scope.list_my_recipes();

        $scope.link_recipe = function(recipe){
            recipe.task = $routeParams.id;
            recipe.$save({'id': recipe.id}).then(function(data){
                $location.path("/marketplace/recipe/"+recipe.id);
            });
        };


})
.controller('recipesCtrl',
    function ($scope, $route, $routeParams, $modal, Recipes, Auth) {
        $scope.user = Auth.getUser();

        $scope.list_recipes = function() {
            Recipes.query().$promise.then(function(data) {
                $scope.recipes = data;

            });
        };

        $scope.list_recipes();

        $scope.create_recipe = function(){
            var modalInstance = $modal.open({
                    templateUrl: 'recipeCreateModalContent.html',
                    controller: 'recipeCreateModalInstanceCtrl',
                    size: 'lg',
                    resolve: {
                        items: function () {
                            var recipe = new Recipes;
                            return recipe;
                        }
                    }
                });

            modalInstance.result.then(function (recipe) {
              $scope.recipe = recipe;
              recipe.$save({'id': recipe.id}).then(function(data){
                  $scope.msg = "Recipe created";
                  $scope.list_recipes();
              },function(data){ $scope.msg = "An error occured, could not create the recipe: "; console.log(data);});
            });
        };


})
.controller('recipeCreateModalInstanceCtrl', function ($scope, $modalInstance, items) {

  $scope.recipe = items;
  $scope.recipe.visible = "true";

  $scope.ok = function () {
    $modalInstance.close($scope.recipe);
  };

  $scope.cancel = function () {
    $modalInstance.dismiss('cancel');
  };
})
.controller('recipeCtrl',
    function ($scope, $route, $routeParams, $location, Recipes, Auth, Task) {
        $scope.user = Auth.getUser();
        $scope.get_recipe = function() {
            Recipes.get({'id': $routeParams.id}).$promise.then(function(data){
                $scope.recipe = data;
                if(data.task !== undefined && data.task!=null) {
                    Task.get({'id': data.task}).$promise.then(function(task_data){
                        $scope.task = task_data;
                    });
                }
            },function() { $scope.msg = "Could not get recipe"});
        };
        $scope.get_recipe();

        $scope.edit = false;

        $scope.edit_recipe = function() {
            $scope.edit = true;
            if($scope.recipe.visible == true) {
                $scope.recipe.visible = "true";
            }
            else {
                $scope.recipe.visible = "false";
            }
        }

        $scope.delete_recipe = function(){
            $scope.recipe.$delete({'id': $scope.recipe.id}).then(function(data){
                $location.path("/marketplace/recipe");
            });
        }

        $scope.cancel = function() {
            $scope.edit = false;
        }

        $scope.ok = function() {
            $scope.edit = false;
            $scope.recipe.$save({'id': $scope.recipe.id}).then(function(data){
                $scope.get_recipe();
            },function(data){ $scope.msg = "An error occured, could not update the recipe: "; console.log(data);});
        }

})
.controller('adminCtrl',
    function ($scope, $route, $modal, $interval, $window, $routeParams, Admin, User) {
        $scope.maintenance = false;
        $scope.formatSizeUnits = function (bytes) {
            if(bytes === undefined) { return ""; }
            if(bytes < 1024) return bytes + " Bytes";
            else if(bytes < 1048576) return(bytes / 1024).toFixed(3) + " KB";
            else if(bytes < 1073741824) return(bytes / 1048576).toFixed(3) + " MB";
            else return(bytes / 1073741824).toFixed(3) + " GB";
        };
        Admin.maintenance().$promise.then(function(data){
            if(data.status.general == 'on') {
                $scope.maintenance = true;
            }
            else {
                $scope.maintenance = false;
            }
        });

        $scope.update_guest_status = function(user) {
                console.log(user);
                User.guest_status({}, user).$promise.then(function(data){});
        };

        $scope.boolToStr = function(arg) {return arg ? true : false};
        User.query().$promise.then(function(data){
            $scope.users = data;
        });

        $scope.switch_maintenance = function(status) {
            if(status == 'off') {
                Admin.goto_maintenance({'id': 'all', 'status': 'off'},{}).$promise.then(function(data){
                    $scope.maintenance = false;
                });
            }
            else {
                Admin.goto_maintenance({'id': 'all', 'status': 'on'},{}).$promise.then(function(data){
                    $scope.maintenance = true;
                });
            }
        }

        $scope.is_outdated = function(UNIX_timestamp) {
            if(UNIX_timestamp == null) {
                return true;
            }
            var last_date = new Date(UNIX_timestamp*1000);
            var current = new Date();
            current.setMinutes(current.getMinutes() - 5);
            if(last_date < current) {
                return true;
            }
            else {
                return false;
            }
        };
        /*
        $scope.remove_host = function(name){
            Admin.unwatch({'id': name}).$promise.then(function(data){
                $scope.refresh();
            });

        };
        */

        $scope.refresh = function() {
            Admin.status({}).$promise.then(function(data){
                $scope.host_status = data;
            });
        }

        var refresh_timer = $interval($scope.refresh, 30000);

        $scope.$on("$destroy", function() {
            $interval.cancel(refresh_timer);
            refresh_timer = undefined;
        });

        $scope.convert_timestamp_to_date = function(UNIX_timestamp){
          if(UNIX_timestamp=='' || UNIX_timestamp===null || UNIX_timestamp===undefined) { return '';}
          var a = new Date(UNIX_timestamp*1000);
          var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
          var year = a.getFullYear();
          var month = months[a.getMonth()];
          var date = a.getDate();
          var hour = a.getHours();
          var min = a.getMinutes();
          var sec = a.getSeconds();
          var time = date + ',' + month + ' ' + year + ' ' + hour + ':' + min + ':' + sec ;
          return time;
        }

        $scope.refresh();

})
.controller('imagesCtrl',
    function ($scope, $route, $modal, $window, $routeParams, Images, GoDConfig) {
        Images.query().$promise.then(function(data) {
            $scope.image_list = data;
        });
})
.controller('projectsCtrl',
    function ($scope, $route, $modal, $window, $routeParams, Projects, GoDConfig) {
        $scope.list_projects = function(){
            Projects.query().$promise.then(function(data){
                $scope.projects = data;
            });
        }
        $scope.list_projects();
        $scope.name = '';
        $scope.description = '';
        $scope.owner = '';
        $scope.volumes = [];
        $scope.prio = 50;
        $scope.show_project = function(project) {
            $scope.project = project;
            $scope.project_members = $scope.project.members;
            if($scope.project.volumes === undefined){
                $scope.project.volumes = [];
            }
        }

        $scope.newvolumename = '';
        $scope.newvolumepath = '';
        $scope.newvolumemount = '';
        $scope.newvolumeacl = 'ro';

        $scope.newmember = '';

        $scope.delete_project = function(project) {
            project.$delete({'id': project.id}).then(function(data){
                $scope.list_projects();
            });
        };
        $scope.update_project = function(project) {
            project.$save({'id': project.id}).then(function(data){
                $scope.list_projects();
            });
        };
        $scope.create_project = function() {
            if($scope.name!==null && $scope.name !== undefined && $scope.name != '') {
                var project = new Projects;
                project.id = $scope.name;
                project.owner = $scope.owner;
                project.volumes = $scope.volumes;
                project.description = $scope.description;
                project.members = [];
                project.$save().then(function(data){
                    $scope.msg = 'Project '+$scope.name+' created';
                    $scope.projects.push(project);
                    $scope.name = '';
                    $scope.description = '';
                    $scope.owner = '';
                    $scope.volumes = [];
                });
            }
            else {
                $scope.msg = "Name is empty";
            }
        };
        $scope.delete_member_project = function(member) {
            var index = $scope.project.members.indexOf(member);
            if (index > -1) {
                $scope.project.members.splice(index, 1);
            }
        };
        $scope.add_member_project = function() {
            if($scope.newmember != '' && $scope.project.members.indexOf($scope.newmember) == -1) {
                $scope.project.members.push($scope.newmember);
            }
        };

        $scope.add_volume_project = function() {
            if($scope.project.owner == ''){
                $scope.msg = 'Owner is empty';
                return;
            }
            if($scope.newvolumename != '' && $scope.newvolumepath != '') {
                if($scope.newvolumemount == '') {
                    $scope.newvolumemount = $scope.newvolumepath;
                }
                $scope.project.volumes.push({
                    'name': $scope.newvolumename,
                    'path': $scope.newvolumepath,
                    'mount': $scope.newvolumemount,
                    'acl': $scope.newvolumeacl
                });
            }
            else {
                $scope.msg = 'Volume name or path is empty';
                return;
            }
        };

        $scope.delete_volume_project = function(index) {
            $scope.project.volumes.splice(index, 1);
        };
})
.controller('jobMonitorCtrl',
    function ($scope, $route, $modal, $window, $routeParams, $interval, Task, GoDConfig) {
        $scope.msg = "";
        $scope.task_id = $routeParams.jobid;
        $scope.options = { 'scaleBeginAtZero': true, 'animation': false};
        $scope.monitor = function(){
            Task.monitor({'id': $scope.task_id}).$promise.then(function(data){
                //console.log(data);
                var stats = null;
                if(data['stats'] !== undefined) {
                    stats = data['stats'];
                }
                else if(data['/docker/'+data.id] === undefined) {
                    if(data['/system.slice/docker-'+data.id+'.scope'] === undefined){
                        var slice = null;
                        for(var k in data) {
                            if(k.indexOf('/system.slice')>-1) {
                                slice = k;
                                break;
                            }

                        }
                        if(slice != null) {
                            stats = data[slice]['stats'];
                        }
                    }
                    else {
                        stats = data['/system.slice/docker-'+data.id+'.scope']['stats'];
                    }
                }
                else {
                    stats = data['/docker/'+data.id]['stats'];
                }
                var labels = [];
                var datatotal = [];
                var datasystem = [];
                var datauser = [];
                var dataaverage = [];
                var mdata = [];
                var nrx = [];
                var ntx = [];
                var samples = stats.length;
                for(var i=1;i<samples;i++){
                     var cdate = new Date(stats[i]['timestamp']);
                     var label = cdate.getHours()+':'+cdate.getMinutes()+':'+cdate.getSeconds();
                     if (samples > 60 && i % 5 != 0) { label = ''; }
                     labels.push(label);
                     //datatotal.push(stats[i]['cpu']['usage']['total']);
                     //datasystem.push(stats[i]['cpu']['usage']['system']);
                     //datauser.push(stats[i]['cpu']['usage']['user']);
                     //dataaverage.push(stats[i]['cpu']['load_average']);
                     var cur = stats[i]['cpu']['usage']['total'];
                     var prev = stats[i-1]['cpu']['usage']['total'];
                     var curDate = new Date(stats[i]['timestamp']);
                     var prevDate = new Date(stats[i-1]['timestamp']);
                     var intervalcpu = (curDate.getTime() - prevDate.getTime()) * 1000000;
                     var value= (cur - prev) / intervalcpu;
                     dataaverage.push(value);

                     mdata.push(stats[i]['memory']['usage']/1000000);
                     nrx.push(stats[i]['network']['rx_bytes'])
                     ntx.push(stats[i]['network']['tx_bytes'])
                }
                $scope.labels = labels;
                //$scope.series = ['total', 'system', 'user'];
                $scope.series = ['load average'];
                //$scope.data = [datatotal, datasystem, datauser];
                $scope.data = [dataaverage];
                $scope.labels = labels;
                $scope.mseries = ['usage (Mb)'];
                $scope.mdata = [mdata];
                $scope.nseries = ['rx_bytes', 'tx_bytes'];
                $scope.ndata = [nrx, ntx];
            },function(){
                $scope.msg = "Could not get info on container, may not be running anymore. If container is new, please retry in a few seconds.";
            });
        };
        var refresh_timer = $interval($scope.monitor, 5000);
        $scope.monitor();

        $scope.$on("$destroy", function() {
            $interval.cancel(refresh_timer);
            refresh_timer = undefined;
        });
})
.controller('jobFilesCtrl',
    function ($scope, $route, $modal, $window, $routeParams, $http, TaskOver, GoDConfig) {
        $scope.task_id = $routeParams.jobid;
        $scope.task_path = [''];
        $scope.files_or_dirs = [];
        TaskOver.files({'id': $scope.task_id, 'path': $scope.task_path.join('/')}).$promise.then(function(data){
            $scope.files_or_dirs = data;
        });
        $scope.bytesToSize = function(bytes) {
           var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
           if (bytes == 0) return '0 Byte';
           var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
           return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
        };
        $scope.goto_file_or_dir = function(file_or_dir) {
            if(file_or_dir.type=='file'){
                $window.open('/api/1.0/task/'+$scope.task_id+"/files"+$scope.task_path.join('/')+'/'+file_or_dir.name, file_or_dir.name);
            }
            else {
                if(file_or_dir.name == '..'){
                    $scope.task_path.pop();
                }
                else {
                    $scope.task_path.push(file_or_dir.name);
                }
                $http.get('/api/1.0/task/'+$scope.task_id+'/files' + $scope.task_path.join('/')).success(function(data){
                //TaskOver.files({'id': $scope.task_id, 'path': $scope.task_path.join('/')}).$promise.then(function(data){
                    if($scope.task_path.length > 1) {
                        data.push({'name': '..', 'path': '..', 'type': 'dir'})
                    }
                    $scope.files_or_dirs = data;
                });
            }
        };

})
.controller('userFilesCtrl',
    function ($scope, $route, $modal, $window, $routeParams, $http, GoDConfig) {
        $scope.task_id = $routeParams.user;
        $scope.task_path = [''];
        $scope.files_or_dirs = [];

        $http.get('/api/1.0/user/'+$scope.task_id+'/files/').success(function(data){
            $scope.files_or_dirs = data;
        })
        $scope.bytesToSize = function(bytes) {
           var sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
           if (bytes == 0) return '0 Byte';
           var i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)));
           return Math.round(bytes / Math.pow(1024, i), 2) + ' ' + sizes[i];
        };
        $scope.goto_file_or_dir = function(file_or_dir) {
            if(file_or_dir.type=='file'){
                $window.open('/api/1.0/user/'+$scope.task_id+"/files"+$scope.task_path.join('/')+'/'+file_or_dir.name, file_or_dir.name);
            }
            else {
                if(file_or_dir.name == '..'){
                    $scope.task_path.pop();
                }
                else {
                    $scope.task_path.push(file_or_dir.name);
                }
                $http.get('/api/1.0/user/'+$scope.task_id+'/files/'+$scope.task_path.join('/')).success(function(data){
                    if($scope.task_path.length > 1) {
                        data.push({'name': '..', 'path': '..', 'type': 'dir'})
                    }
                    $scope.files_or_dirs = data;
                })
            }
        };

})
.factory('mySocket', function ($rootScope) {
  var socket = null;
  return {
    is_connected: function() {
        return socket !== null;
    },
    connect: function(dest) {
      socket = io.connect(dest);
    },
    on: function (eventName, callback) {
      socket.on(eventName, function () {
        var args = arguments;
        $rootScope.$apply(function () {
          callback.apply(socket, args);
        });
      });
    },
    once: function (eventName, callback) {
      socket.once(eventName, function () {
        var args = arguments;
        $rootScope.$apply(function () {
          callback.apply(socket, args);
        });
      });
    },
    emit: function (eventName, data, callback) {
      socket.emit(eventName, data, function () {
        var args = arguments;
        $rootScope.$apply(function () {
          if (callback) {
            callback.apply(socket, args);
          }
        });
      })
    }
  };
})
.controller('jobLiveCtrl',
    function ($scope, $routeParams, mySocket, TaskOver, Task, GoDConfig) {
        GoDConfig.get().$promise.then(function(config) {
            Task.token({'id': $routeParams.jobid}).$promise.then(function(token) {
                $scope.linenumber = 1;
                if(config['live']['status'] !== undefined && config['live']['status']){
                    mySocket.emit('message', {'type': 'tail', 'token': token});
                }
            });
        });
        $scope.$on("$destroy", function() {
            mySocket.emit('message', {'type': 'untail'});
        });

    })
.controller('jobsCtrl',
    function ($scope, $route, $modal, $interval, $location, $document, growl,
              mySocket, Auth, Task, TaskActive, TaskOver, GoDConfig, DTOptionsBuilder, DTInstances, Admin, User) {

        $scope.limit = 1000;
        $scope.maintenance = false;
        Admin.maintenance().$promise.then(function(data){
            if(data.status.general == 'on') {
                $scope.maintenance = true;
            }
            else {
                $scope.maintenance = false;
            }
        });

        $scope.sort = {
                    sortingOrder : 'id',
                    reverse : false
                };
        $scope.pageSize = 10;
        $scope.page_sizes = ['10', '20', '50', '100'];
        $scope.nb_pages = 0;
        $scope.currentPage = 0;
        $scope.nb_pages = 0;
        $scope.regex = null;

        $scope.search_with_regex = function(){
                $scope.load_jobs_paginated();
        };

        $scope.sort_by = function(field) {
            if($scope.sort['sortingOrder'] == field) {
                    $scope.sort['reverse'] = ! $scope.sort['reverse']
            }
            else {
                $scope.sort['sortingOrder'] = field;
                $scope.sort['reverse'] = false;
            }
            $scope.currentPage = 0;
            $scope.load_jobs_paginated();
        };

        $scope.setPageSize = function(){
            console.log("refresh with page size " + $scope.pageSize);
            $scope.load_jobs_paginated();
        }

        $scope.getNumberAsArray = function (num) {
            return new Array(num);
        };

        $scope.numberOfPages = function() {
            var nb_pages =  Math.ceil($scope.nb_jobs/ $scope.pageSize);
            return nb_pages;
        };

        var searchMatch = function (haystack, needle) {
            if (!needle) {
                return true;
            }
            return haystack.toLowerCase().indexOf(needle.toLowerCase()) !== -1;
        };

        $scope.prevPage = function () {
            if ($scope.currentPage > 0) {
                $scope.currentPage--;
                $scope.load_jobs_paginated();
            }
        };

        $scope.nextPage = function () {
            if ($scope.currentPage < $scope.nb_pages - 1) {
                $scope.currentPage++;
                $scope.load_jobs_paginated();
            }
        };

        $scope.load_jobs_paginated = function() {
            TaskOver.query({'project': $scope.project, 'draw': 1,
                            'start': $scope.currentPage*$scope.pageSize,
                            'limit': $scope.pageSize,
                            'orderBy': $scope.sort['sortingOrder'],
                            'reverse': $scope.sort['reverse'],
                            'regex' : $scope.regex
                        }).$promise.then(function(data) {
                                $scope.overtasks = data[0].data;
                                $scope.nb_jobs = data[0].recordsTotal;
                                $scope.nb_pages = $scope.numberOfPages();
                        });
        }

        $scope.setCurrentPage = function (page) {
            $scope.currentPage = page;
            $scope.load_jobs_paginated();
        };


        $scope.get_all_tasks = false;
        $scope.user = Auth.getUser();

        $scope.project = 'my';

        User.projects({'id': $scope.user['id']}).$promise.then(function(data){
            $scope.projects = data;
            $scope.projects.unshift({'id': 'my'});
        });

        $scope.autorefresh = 'Off';

        $scope.features = null;
        $scope.live = false;

        $scope.change_autorefresh = function(){
            if($scope.autorefresh == 'Off') {
                $scope.autorefresh = 'On';
            }
            else {
                $scope.autorefresh = 'Off';
            }
        }

        $scope.searchTask = function() {
            Task.get({'id': $scope.searchedTask}).$promise.then(function(task){
                $scope.show_task(task);
            });
        };

        GoDConfig.get().$promise.then(function(data) {
            $scope.networks = data['defaults']['network']
            $scope.features = data.executor_features;
            $scope.cadvisor = data['cadvisor'];
            $scope.live = data['live']['status'];
            if(data['live']['status'] !== undefined && data['live']['status']){
                if(! mySocket.is_connected()) {
                    mySocket.connect(data['live']['url']);
                }
                mySocket.on('connect', function () {
                    mySocket.on('taskover', function (data) {
                      if(data.status == 'killed') {
                          growl.addWarnMessage("Task "+data.id+" "+data.status);
                      }
                      else {
                          growl.addSuccessMessage("Task "+data.id+" "+data.status);
                      }
                    });
                    var first_data = false;
                    mySocket.on('first-data', function (data) {
                        first_data = true;
                        var lines = data.split('\n');
                        angular.element($document[0].getElementById('livelog')).html('');
                        for(var i=0;i<lines.length;i++) {
                            angular.element($document[0].getElementById('livelog')).append('<div class="liveline"><div class="livedata"><pre>'+lines[i]+'</pre></div></div>');
                        }
                    });
                    mySocket.on('new-data', function (data) {
                        /*
                        if(first_data == true){
                            angular.element($document[0].getElementById('livelog')).html('');
                            first_data = false;
                        }
                        */
                        var newLine = '<pre>'+data+'</pre>';
                        angular.element($document[0].getElementById('livelog')).append(newLine);
                    });

                    mySocket.emit('message', {'type': 'authenticate', 'user': Auth.getUser()['id']});

                });
            }
        });
        $scope.dtOptions = DTOptionsBuilder.newOptions().withOption('order', [0, 'desc']).withPaginationType('full_numbers').withDisplayLength(10);

        $scope.auto_refresh = function(){

            if($scope.autorefresh == 'Off') {
                    return;
            }
            $scope.refresh();
        }
        $scope.refresh = function(){

            if($scope.get_all_tasks) {
                TaskActive.get_all({'limit': $scope.limit}).$promise.then(function(data) {
                    $scope.activetasks = data;

                });
            }
            else {
                TaskActive.query({'limit': $scope.limit, 'project': $scope.project}).$promise.then(function(data) {
                    $scope.activetasks = data;

                });
            }
            TaskOver.query({'project': $scope.project, 'draw': 1}).$promise.then(function(data) {
                    $scope.overtasks = data[0].data;
                    $scope.nb_jobs = data[0].recordsTotal;
                    $scope.nb_pages = $scope.numberOfPages();
            });

        };

        var refresh_timer = $interval($scope.auto_refresh, 10000);

        $scope.$on("$destroy", function() {
            $interval.cancel(refresh_timer);
            refresh_timer = undefined;
        });

        $scope.suspend = function(task_id) {
            Task.suspend({'id': task_id}).$promise.then(function(data){
                task['status'] = data['status'];
                $scope.refresh();
            });
        };

        $scope.resume = function(task_id) {
            Task.resume({'id': task_id}).$promise.then(function(data){
                task['status'] = data['status'];
                $scope.refresh();
            });
        };

        $scope.reschedule = function(task_id) {
            Task.reschedule({'id': task_id}).$promise.then(function(data){
                task['status'] = data['status'];
                $scope.refresh();
            });
        };
        /*
        DTInstances.getList().then(function (dtInstances) {
            //console.log(dtInstances);
            $scope.dtInstanceRunning = dtInstances['running_job_list'];
            $scope.dtInstanceOver = dtInstances['over_job_list'];
            $scope.refresh();
        });
        */
        $scope.refresh();

        $scope.replay = function(task_id) {
                $location.path("/job/replay/"+task_id);
        };

        $scope.get_exitcode_class = function(status) {
            var exitcode = status.exitcode;
            if(exitcode === undefined) {
                return 'label label-warning';
            }
            if(exitcode>0 && exitcode!=137){
                if(status.reason !== undefined && status.reason !=null && status.reason != '') {
                    return 'label label-fatal';
                }
                else {
                    return 'label label-danger';
                }
            }
            if(exitcode == 137) {
                return 'label label-warning';
            }
            else {
                return 'label label-success';
            }
        };

        $scope.kill_task = function(task){
            Task.kill({'id': task['id']}).$promise.then(function(data){
                task['status'] = data['status'];
                $scope.refresh();
            });
        };

        $scope.show_task = function(task){
            task['live'] = $scope.live;
            var modalInstance = $modal.open({
                    templateUrl: 'myTaskModalContent.html',
                    controller: 'TaskModalInstanceCtrl',
                    size: 'lg',
                    resolve: {
                        items: function () {
                            return task;
                        }
                    }
                });

            modalInstance.result.then(function (selectedItem) {
              $scope.selectedtask = selectedItem;
            });
        };

        $scope.show_connect_task = function(task){
            task['live'] = $scope.live;
            var modalInstance = $modal.open({
                    templateUrl: 'myTaskConnectModalContent.html',
                    controller: 'TaskConnectModalInstanceCtrl',
                    size: 'md',
                    resolve: {
                        items: function () {
                            return task;
                        }
                    }
                });

            modalInstance.result.then(function (selectedItem) {
              $scope.selectedtask = selectedItem;
            });
        };


        $scope.monitor = function(task_id){
                $location.path('/job/'+task_id+"/monitor");
        };

})
.controller('TaskConnectModalInstanceCtrl', function ($scope, $modalInstance, items) {

    $scope.selectedtask = items;
    $scope.ssh_port = null;
    if(items.command.interactive){
        if(items.container.port_mapping && items.container.port_mapping.length > 0) {
            for(var i=0;i<items.container.port_mapping.length;i++) {
                if(items.container.port_mapping[i].container == 22) {
                    $scope.ssh_port = items.container.port_mapping[i].host;
                }
            }
        }
        else {
            $scope.ssh_port = 22;
        }
    }

  $scope.ok = function () {
    //$modalInstance.close($scope.selectedtask);
  };

  $scope.cancel = function () {
    $modalInstance.dismiss('cancel');
  };
})
.controller('TaskModalInstanceCtrl', function ($scope, $modalInstance, Task, items) {

    $scope.codemirror_options = {
            lineNumbers: true,
            theme:'twilight',
            readOnly: true,
            lineWrapping : true,
            mode: 'text/plain',
            onLoad: function(_editor){
                var editor = _editor;
                setTimeout(function(){
                    editor.refresh();
                }, 100);
            }
        };
    $scope.selectedtask = items;
    $scope.ssh_port = null;
    if(items.command.interactive){
        if(items.container.port_mapping && items.container.port_mapping.length > 0) {
            for(var i=0;i<items.container.port_mapping.length;i++) {
                if(items.container.port_mapping[i].container == 22) {
                    $scope.ssh_port = items.container.port_mapping[i].host;
                }
            }
        }
        else {
            $scope.ssh_port = 22;
        }
    }
    $scope.new_task = false;

    $scope.archive = function(id){
        Task.archive({'id' : id}).$promise.then(function(data){
            $modalInstance.dismiss('cancel');
        });
    }

  $scope.convert_timestamp_to_date = function(UNIX_timestamp){
    if(UNIX_timestamp=='' || UNIX_timestamp===null || UNIX_timestamp===undefined) { return '';}
    var a = new Date(UNIX_timestamp*1000);
    var months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
    var year = a.getFullYear();
    var month = months[a.getMonth()];
    var date = a.getDate();
    var hour = a.getHours();
    var min = a.getMinutes();
    var sec = a.getSeconds();
    var time = date + ',' + month + ' ' + year + ' ' + hour + ':' + min + ':' + sec ;
    return time;
  }

  $scope.ok = function () {
    //$modalInstance.close($scope.selectedtask);
  };

  $scope.cancel = function () {
    $modalInstance.dismiss('cancel');
  };
})
.controller('jobCtrl',
    function ($scope, $route, $routeParams, $location, GoDConfig, Task, User, Auth, Images) {
        var task_id = $routeParams.id;
        Images.query().$promise.then(function(data) {
           $scope.image_list = data;
        });

        $scope.placement = null;
        $scope.network = 'public';
        $scope.selectedconstraint = null;
        $scope.selectedconstraints = [];
        $scope.array_values = null;
        $scope.parent_ids = '';
        $scope.image = '';
        $scope.tags = '';
        $scope.taskname = '';
        $scope.interactive = false;
        $scope.notify = false;
        $scope.volumes = [];
        $scope.needroot = false;
        $scope.ram = 8;
        $scope.cpu = 1;
        $scope.cpu_options = {
            min: 1,
            max: 100,
            step: 1
        };
        $scope.project = {'id': 'default'};
        $scope.projects = [];
        $scope.description = '';
        $scope.tmpstorage = '';
        $scope.ports = null;
        $scope.taskuris = null;
        $scope.failure_policy = 0;

        $scope.add_constraint = function() {
            $scope.selectedconstraints.push($scope.selectedconstraint);
        };
        $scope.delete_constraint = function(constraint) {
            var index = $scope.selectedconstraints.indexOf(constraint);
            if (index > -1) {
                $scope.selectedconstraints.splice(index, 1);
            }
        };

        var user = Auth.getUser();
        User.projects({'id': user['id']}).$promise.then(function(data){
            $scope.projects = data;
        });
        $scope.interactive_images = [];
        $scope.taskcommand = "#!/bin/bash\n\n#Commands to execute, available environment variables\n#GODOCKER_JID: Job identifier\n#GODOCKER_PWD: Job directory, container will execute script in this directory\n#GODOCKER_HOME: mount path for user home directory.\n#GODOCKER_DATA: personal storage (read only)\n#GODOCKER_TASK_ID: Job array task identifier\n#GODOCKER_TASK_FIRST: First job array task identifier\n#GODOCKER_TASK_LAST: Last job array task identifier\n#GODOCKER_TASK_STEP: steps in job array identifiers\n\necho HelloGODOCKER\n";

        GoDConfig.get().$promise.then(function(data) {
            $scope.placement = data['executors_placement'][0];
            $scope.executors_placement = data['executors_placement'];
            $scope.networks = data['defaults']['network'];
            $scope.config_volumes = data['volumes'];
            $scope.config_images = data['images'];
            $scope.config_allow_root = data['defaults']['allow_root'];
            $scope.config_allow_user_images = data['defaults']['allow_user_images'];
            $scope.config_tmpstorage = data['defaults']['tmpstorage'];
            $scope.failure_policy = 0;
            $scope.defaults_failure_policy = data['defaults']['failure_policy'];
            var private_registry = data['defaults']['use_private_registry'];
            var interactive_images = [];
            if($scope.config_images.length>0) {
                $scope.image = $scope.config_images[0];
                for(var i=0;i<$scope.config_images.length;i++) {
                    if($scope.config_images[i].default !== undefined && $scope.config_images[i].default) {
                        $scope.image = $scope.config_images[i];
                    }
                    if($scope.config_images[i].interactive){
                        interactive_images.push($scope.config_images[i]);
                    }
                }
                $scope.interactive_images = interactive_images;
                $scope.cpu = data['defaults']['cpu'];
                $scope.ram = data['defaults']['ram'];
            }
            var constraints = [];
            var nbconstraints = data['constraints'].length;
            for(var i=0;i<nbconstraints;i++) {
                for(var j=0;j<data['constraints'][i]['value'].length;j++) {
                    constraints.push(data['constraints'][i]['name']+'=='+data['constraints'][i]['value'][j]);
                }
            }
            $scope.constraints = constraints;
            if(task_id !== undefined){
                Task.get({'id': task_id}, function(data){
                    var image_in_config = false;
                    for(var i=0;i<$scope.config_images.length;i++){
                        if(data['container']['image'] == $scope.config_images[i].url) {
                            $scope.image = $scope.config_images[i];
                            image_in_config = true;
                            break;
                        }
                    }
                    if(! image_in_config) {
                        $scope.image = { 'url': data['container']['image'] };
                    }
                    if(private_registry !== undefined && private_registry != null) {
                        $scope.image['url'] = $scope.image['url'].replace(private_registry + '/', '')
                    }
                    $scope.project = {'id': data['user']['project']};
                    $scope.taskname = data['meta']['name'];
                    $scope.description = data['meta']['description'];
                    $scope.interactive = data['command']['interactive'];
                    $scope.network = data['requirements']['network'];
                    var volumes = [];
                    for(var i=0;i<$scope.config_volumes.length;i++){
                        for(var j=0;j<data['container']['volumes'].length;j++){
                            if($scope.config_volumes[i]['name'] == data['container']['volumes'][j]['name']) {
                                volumes.push($scope.config_volumes[i]);
                                break;
                            }
                        }
                    }
                    //$scope.volumes = data['container']['volumes'];
                    $scope.volumes = volumes;
                    $scope.needroot = data['container']['root'];
                    if(data['requirements']['tmpstorage']) {
                        $scope.tmpstorage = data['requirements']['tmpstorage']['size'];
                    }
                    if(data['requirements']['ports']) {
                        $scope.ports = data['requirements']['jobs'].join(',')
                    }
                    $scope.ram = data['requirements']['ram'];
                    $scope.cpu = data['requirements']['cpu'];
                    $scope.cpu_options = {
                        min: 1,
                        max: 100,
                        step: 1
                    };
                    if(data['requirements']['executor'] !== undefined) {
                        $scope.placement = data['requirements']['executor'];
                    }
                    $scope.parent_ids = data['requirements']['tasks'].join(',');
                    $scope.array_values = data['requirements']['array']['values'];
                    $scope.tags = data['meta']['tags'].join(',');
                    $scope.taskcommand = data['command']['cmd'];
                    $scope.selectedconstraints = data['requirements']['label'];
                    if(data['requirements']['uris']!==undefined && data['requirements']['uris']!=null) {
                        $scope.taskuris = data['requirements']['uris'].join("\n");
                    }
                });
            }
        });

        $scope.askroot = function(){
            //console.log($scope.needroot);
        }

        $scope.new_job = function() {
            var task = new Task;
            task.id = null;
            if($scope.array_values && $scope.array_values.split(':').length!=3){
                alert('Wrong job array format: start:end:step');
            }
            //task.date = Date.now();
            if(task.user===undefined){
                    task['user'] = {}
            }
            task.user.project = $scope.project.id;
            task.meta = {
                        'name': $scope.taskname,
                        'description': $scope.description,
                        'tags': $scope.tags.split(','),
                    };
            task.notify = {
                'email': $scope.notify
            }
            var task_req_parents = $scope.parent_ids.split(',');
            if($scope.parent_ids == "") {
                task_req_parents = [];
            }
            task.requirements =  {
                        'cpu': $scope.cpu,
                        'ram': $scope.ram,
                        'array': {
                            'values': $scope.array_values
                        },
                        'label': $scope.selectedconstraints,
                        'tasks': task_req_parents,
                        'failure_policy': parseInt($scope.failure_policy),
                        'executor': $scope.placement
                    };
            if($scope.network != null && $scope.network != "") {
                task.requirements['network'] = $scope.network;
            }
            if($scope.taskuris != null && $scope.taskuris != "") {
                task.requirements['uris'] = $scope.taskuris.split("\n");
            }
            if($scope.ports) {
                task.requirements['ports'] = [];
                var ports = $scope.ports.split(',')
                for(var i=0;i<ports.length;i++) {
                    task.requirements['ports'].push(parseInt(ports[i].trim()));
                }
            }
            if($scope.tmpstorage!==null && $scope.tmpstorage != '') {
                task.requirements['tmpstorage'] = { 'size': $scope.tmpstorage, 'path': null };
            }

            task.container = {
                        'image': $scope.image.url,
                        'volumes': $scope.volumes,
                        'network': true,
                        'id': null,
                        'meta': null,
                        'stats': null,
                        'ports': [],
                        'root': $scope.needroot
                    };
            task.command = {
                        'interactive': $scope.interactive,
                        'cmd': $scope.taskcommand
                    };
            task.status = {
                        'primary': null,
                        'secondary': null
                    };
            // console.log(task);

            task.$save().then(function(data){
                $location.path('/jobs');
            });


        };
})
.controller('userCtrl',
    function($scope, $rootScope, $routeParams, $log, $location, $window, Auth, User) {

        $scope.is_logged = false;
        $rootScope.$on('loginCtrl.login', function (event, user) {
           $scope.user = user;
           $scope.is_logged = true;
        });


        $scope.logout = function() {
            User.logout().$promise.then(function(){
                Auth.setUser(null);
                $scope.user = null;
                $scope.is_logged = false;
                delete $window.sessionStorage.token;
                $location.path('/login');
            });
        };
        User.is_authenticated().$promise.then(function(user) {
            if(user !== null && user['id'] !== undefined) {
                $scope.user = user;
                $scope.is_logged = true;
                Auth.setUser($scope.user);
            }
        });

})
.controller('userInfoCtrl',
    function ($scope, $rootScope, $route, $routeParams, $location, $http, Auth, User, GoDConfig, Projects) {
        var requested_user = $routeParams.user;
        var user = Auth.getUser();
        $scope.user = user;
        $scope.formatSizeUnits = function (bytes) {
            if(bytes === undefined) { return ""; }
            if(bytes < 1024) return bytes + " Bytes";
            else if(bytes < 1048576) return(bytes / 1024).toFixed(3) + " KB";
            else if(bytes < 1073741824) return(bytes / 1048576).toFixed(3) + " MB";
            else return(bytes / 1073741824).toFixed(3) + " GB";
        };
        GoDConfig.get().$promise.then(function(config) {
            $scope.config = config;
            $scope.ftp_quota = config.ftp.quota;
        });
        User.info({'id': requested_user}).$promise.then(function(data){
            $scope.credentials = data.credentials
            if(data.usage.prio === undefined) {
                $scope.prio = 50;
            }
            else {
                $scope.prio = data.usage.prio;
            }
            $scope.quota_time = data.usage.quota_time;
            $scope.quota_cpu = data.usage.quota_cpu;
            $scope.quota_ram = data.usage.quota_ram;
            $scope.guest_home = data.usage.guest_home;
            User.usage({'id': requested_user}).$promise.then(function(data){
                 var used_cpu = 0;
                 var used_ram = 0;
                 var used_time = 0;
                 for(var i=0;i<data.length;i++) {
                    used_cpu += data[i]['cpu'];
                    used_ram += data[i]['ram'];
                    used_time += data[i]['time'];
                 }
                 $scope.used_cpu = 0;
                 //console.log(data);
                 if($scope.quota_cpu>0) {
                     $scope.used_cpu = Math.floor(used_cpu*100/$scope.quota_cpu);
                 }
                 $scope.used_ram = 0;
                 if($scope.quota_ram>0) {
                     $scope.used_ram = Math.floor(used_ram*100/$scope.quota_ram);
                 }
                 $scope.used_time = 0;
                 if($scope.quota_time>0) {
                     $scope.used_time = Math.floor(used_time*100/$scope.quota_time);
                 }
            });
            $scope.projects_usage = {};
            User.projects({'id': requested_user}).$promise.then(function(data){
                //$scope.projects = data;
                for(var i=0;i<data.length;i++){
                    if(data[i]['id']!='default'){
                        var project = data[i];
                        Projects.usage({'id': project['id']}).$promise.then(function(data) {
                            var used_cpu = 0;
                            var used_ram = 0;
                            var used_time = 0;
                            for(var i=0;i<data.length;i++) {
                               used_cpu += project['cpu'];
                               used_ram += project['ram'];
                               used_time += project['time'];
                            }
                            $scope.projects_usage[project['id']] = { 'id': project['id']};
                            $scope.projects_usage[project['id']].used_cpu = 0;
                            //console.log(data);
                            if($scope.quota_cpu>0) {
                                $scope.projects_usage[project['id']].used_cpu = Math.floor(used_cpu*100/$scope.quota_cpu);
                            }
                            $scope.projects_usage[project['id']].used_ram = 0;
                            if($scope.quota_ram>0) {
                                $scope.projects_usage[project['id']].used_ram = Math.floor(used_ram*100/$scope.quota_ram);
                            }
                            $scope.projects_usage[project['id']].used_time = 0;
                            if($scope.quota_time>0) {
                                $scope.projects_usage[project['id']].used_time = Math.floor(used_time*100/$scope.quota_time);
                            }
                        });
                    }
                }
            });
            var ftp_used_quota = 0;
            for (var key in data.ftp.quota) {
                ftp_used_quota += data.ftp.quota[key];
            }
            $scope.ftp_used_quota = ftp_used_quota;

        });

        $scope.apikey_renew = function() {
            User.apikey_renew({'id': $scope.user['id']}).$promise.then(function(data){
                $scope.credentials.apikey = data.apikey;
            });
        };

        $scope.update_info = function() {
            User.update_info({'id': $scope.user['id']},
                            {'credentials': $scope.credentials,
                             'usage': { 'prio': $scope.prio,
                                         'quota_time': $scope.quota_time,
                                         'quota_cpu': $scope.quota_cpu,
                                         'quota_ram': $scope.quota_ram,
                                     }
                    }).$promise.then(function(data){
            });
        };

        $scope.apps = null;
        $scope.app_name = null;
        $scope.app_list = function() {
            $http.get('/api/1.0/3rdparty/app').success(function(data){
                $scope.apps = data;
            })
        };
        $scope.app_create = function() {
            $http.post('/api/1.0/3rdparty/app', {'name': $scope.app_name}).success(function(data){
                $scope.app_list();
            })
        };
        $scope.app_delete = function(app) {
            $http.delete('/api/1.0/3rdparty/app/'+ app['_id']['$oid']).success(function(data){
                $scope.app_list();
            })
        };
        $scope.app_list();
})
.controller('loginCtrl',
    function ($scope, $rootScope, $route, $routeParams, $location, $window, Auth, User, GoDConfig) {
        $scope.uid = "";
        $scope.password = "";
        GoDConfig.get().$promise.then(function(config) {
            $scope.config = config;
        });
        if($routeParams.guest_token !== undefined) {
            User.guest_authenticate({},{'token': $routeParams.guest_token}).$promise.then(function(data){
                var user = data['user'];
                if(! user['guest_status']) {
                    $scope.msg = 'Your account is not active, please wait for its activation by administrators'
                    return;
                }
                if($window.sessionStorage != null) {
                    $window.sessionStorage.token = data.token;
                }
                if(user['id'] !== undefined) {
                    Auth.setUser(user);
                    $rootScope.$broadcast('loginCtrl.login', user);
                    $location.path('/');
                }
                else{
                    $scope.msg = "Could not authenticate!";
                }
            }, function(data) {
                if($window.sessionStorage != null) {
                    delete $window.sessionStorage.token;
                }
                $scope.msg = "Could not authenticate!";
            });
        }

        $scope.authenticate = function() {
            User.authenticate({},{'uid':$scope.uid, 'password':$scope.password}).$promise.then(function(data){
                var user = data['user'];
                if($window.sessionStorage != null) {
                    $window.sessionStorage.token = data.token;
                }
                if(user['id'] !== undefined) {
                    Auth.setUser(user);
                    $rootScope.$broadcast('loginCtrl.login', user);
                    $location.path('/');
                }
                else{
                    $scope.msg = "Could not authenticate!";
                }
            }, function(data) {
                if($window.sessionStorage != null) {
                    delete $window.sessionStorage.token;
                }
                $scope.msg = "Could not authenticate!";
            });

        };
})
.service('Auth',
    function(){
        var user =null;
        return {
            getUser: function() {
                return user;
            },
            setUser: function(newUser) {
                user = newUser;
            },
            isConnected: function() {
                return !!user;
            }
        };
});
