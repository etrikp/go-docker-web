/*jslint sub:true, browser: true, indent: 4, vars: true, nomen: true */

(function () {
  'use strict';

    function Stat($resource) {
        return $resource('../api/1.0/task', {}, {
            count_status: {
                url: '../api/1.0/task/status/:status/count',
                method: 'GET',
                isArray: false,
                cache: false
            }
        });
    }

    function User($resource) {
        return $resource('../api/1.0/user/:uid', {}, {
            info: {
                url: '../api/1.0/user/:id',
                method: 'GET',
                isArray: false,
                cache: false
            },
            usage: {
                url: '../api/1.0/user/:id/usage',
                method: 'GET',
                isArray: true,
                cache: false
            },
            update_info:{
                url: '../api/1.0/user/:id',
                method: 'PUT',
                isArray: false,
                cache: false
            },
            is_authenticated: {
                url: '../user/logged',
                method: 'GET',
                isArray: false,
                cache: false
            },
            authenticate: {
                url: '../user/bind',
                method: 'POST',
                isArray: false,
                cache: false
            },
            guest_authenticate: {
                url: '../guest/bind',
                method: 'POST',
                isArray: false,
                cache: false
            },
            guest_status: {
                url: '../guest/status',
                method: 'POST',
                isArray: false,
                cache: false
            },
            logout: {
                url: '../user/logout',
                method: 'GET',
                isArray: false,
                cache: false
            },
            projects: {
                url: '../api/1.0/user/:id/project',
                method: 'GET',
                isArray: true,
                cache: false
            },
            apikey_renew: {
                url: '../api/1.0/user/:id/apikey',
                method: 'DELETE',
                isArray: false,
                cache: false
            }
        });
    }

    function GoDConfig($resource) {
        return $resource('../api/1.0/config');
    }

    function GoDUsage($resource) {
        return $resource('../api/1.0/usage', {}, {
            get: {
                url: '../api/1.0/usage',
                method: 'GET',
                isArray: true,
                cache: false
            }
        });
    }

    function Projects($resource) {
        return $resource('../api/1.0/project/:id', {}, {
            usage: {
                url: '../api/1.0/project/:id/usage',
                method: 'GET',
                isArray: true,
                cache: false
            }
        });
    }

    function Images($resource) {
        return $resource('../api/1.0/image/:id');
    }

    function Recipes($resource) {
        return $resource('../api/1.0/marketplace/recipe/:id');
    }

    function Task($resource) {
        return $resource('../api/1.0/task/:id', {}, {
            archive: {
                url: '../api/1.0/task/:id/archive',
                method: 'GET',
                isArray: false,
                cache: false
            },
            kill: {
                url: '../api/1.0/task/:id',
                method: 'DELETE',
                isArray: false,
                cache: false
            },
            monitor: {
                url: '../api/1.0/task/:id/monitor',
                method: 'GET',
                isArray: false,
                cache: false
            },
            token: {
                url: '../api/1.0/task/:id/token',
                method: 'GET',
                isArray: false,
                cache: false
            },
            suspend: {
                url: '../api/1.0/task/:id/suspend',
                method: 'GET',
                isArray: false,
                cache: false
            },
            resume: {
                url: '../api/1.0/task/:id/resume',
                method: 'GET',
                isArray: false,
                cache: false
            },
            reschedule: {
                url: '../api/1.0/task/:id/reschedule',
                method: 'GET',
                isArray: false,
                cache: false
            }
        });
    }

    function TaskActive($resource) {
        return $resource('../api/1.0/task/active', {}, {
            get_all: {
                url: '../api/1.0/task/active/all',
                method: 'GET',
                isArray: true,
                cache: false
            }
        });
    }

    function TaskOver($resource) {
        return $resource('../api/1.0/task/over', {}, {
            files: {
                url: '../api/1.0/task/:id/files/:path',
                method: 'GET',
                isArray: true,
                cache: false
            }
        });
    }

    function Admin($resource) {
        return $resource('../admin', {}, {
            status: {
                url: '../admin/status',
                method: 'GET',
                isArray: true,
                cache: false
            },
            unwatch: {
                url: '../admin/proc/:id',
                method: 'DELETE',
                isArray: false,
                cache: false
            },
            maintenance: {
                url: '../admin/maintenance',
                method: 'GET',
                isArray: false,
                cache: false
            },
            goto_maintenance: {
                url: '../admin/maintenance/:id/:status',
                method: 'POST',
                isArray: false,
                cache: false
            }
        });
    }

  angular.module('god.resources', ['ngResource'])
      .factory('GoDConfig', GoDConfig)
      .factory('Projects', Projects)
      .factory('Admin', Admin)
      .factory('User', User)
      .factory('Task', Task)
      .factory('TaskActive', TaskActive)
      .factory('TaskOver', TaskOver)
      .factory('Stat', Stat)
      .factory('GoDUsage', GoDUsage)
      .factory('Recipes', Recipes)
      .factory('Images', Images);

}());
