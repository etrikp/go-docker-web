import unittest
import datetime
import time
import string
import random
import sys
import os
import tempfile
from pyramid.paster import get_app

import json
from bson.json_util import dumps
import jwt

import godocker.utils as godutils

class FunctionalTests(unittest.TestCase):
    def setUp(self):
        shared_dir = tempfile.mkdtemp('godshared')
        # shared_dir = os.path.join(dirname, '..', 'godshared')
        os.environ['GODOCKER_SHARED_DIR'] = shared_dir
        self.app = get_app('testing.ini')
        from webtest import TestApp
        self.testapp = TestApp(self.app)
        self.api = '1.0'
        self.user = {
            'id': 'sample',
            'last': datetime.datetime.now(),
            'credentials': {
                'apikey': ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(10)),
                'private': '',
                'public': ''
            },
            'uid': 1001,
            'gid': 1001,
            'sgids': [],
            'homeDirectory': '/home/sample',
            'email': 'fakeemail@no.mail',
            'usage':
            {
                'time': 0,
                'cpu': 0,
                'ram': 0,
                'prio': 50
            },
            'projects': []
        }
        self.other_user = {
            'id': 'invite',
            'last': datetime.datetime.now(),
            'credentials': {
                'apikey': ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(10)),
                'private': '',
                'public': ''
            },
            'uid': 1001,
            'gid': 1001,
            'sgids': [],
            'homeDirectory': '/home/sample',
            'email': 'fakeemail@no.mail',
            'usage':
            {
                'time': 0,
                'cpu': 0,
                'ram': 0,
                'prio': 50
            },
            'projects': []
        }
        dt = datetime.datetime.now()
        self.task = {
               'user': {
                   'id': self.user['id'],
                   'uid': self.user['uid'],
                   'gid': self.user['gid']
               },
               'date': time.mktime(dt.timetuple()),
               'meta': {
                   'name': 'some_name',
                   'description': 'blabla',
                   'tags': []
               },
               'requirements': {
                   'cpu': 1,
                   # In Gb
                   'ram': 1,
                   'array': { 'values': None}
               },
               'container': {
                   'image': 'centos:latest',
                   'volumes': [],
                   'network': True,
                   'id': None,
                   'meta': None,
                   'stats': None,
                   'ports': [],
                   'root': False
               },
               'command': {
                   'interactive': False,
                   'cmd': '/bin/sleep 30'
               },
               'status': {
                   'primary': None,
                   'secondary': None
               }
           }

        self.app.registry.db_redis.flushdb()
        self.app.registry.db_mongo['jobs'].drop()
        self.app.registry.db_mongo['jobsover'].drop()
        self.app.registry.db_mongo['users'].drop()
        self.app.registry.db_mongo['projects'].drop()
        self.app.registry.db_mongo['users'].insert(self.user)
        self.app.registry.db_mongo['users'].insert(self.other_user)

    def tearDown(self):
        self.app.registry.db_redis.flushdb()
        self.app.registry.db_mongo['jobs'].drop()
        self.app.registry.db_mongo['jobsover'].drop()
        self.app.registry.db_mongo['projects'].drop()
        self.app.registry.db_mongo['users'].drop()

    def test_user_unauthenticated(self):
        res = self.testapp.get('/api/1.0/user/'+str(self.user['id']), status=401)
        self.assertTrue(res.status_int == 401)

    def test_config(self):
        res = self.testapp.get('/api/1.0/config', status=200)
        json_res = res.json
        self.assertTrue(len(json_res['images'])>0)

    def test_wrong_authenticate(self):
        #/api/1.0/authenticate
        res = self.testapp.post('/api/'+self.api+'/authenticate', dumps({'user': self.user['id'], 'apikey': 'abcd'}), status=401)

    def test_authenticate(self,as_user=None):
        user = self.user
        if as_user is not None:
            user = as_user

        #/api/1.0/authenticate
        res = self.testapp.post('/api/'+self.api+'/authenticate', dumps({'user': user['id'], 'apikey': user['credentials']['apikey']}), status=200)
        json_res = res.json
        self.assertTrue('token' in json_res)
        # Return ASCII representation, not unicode
        python_version = sys.version_info.major
        if python_version == 2:
            json_res['token'] = json_res['token'].encode('ascii','ignore')
        else:
            if not isinstance(json_res['token'], str) and  not isinstance(json_res['token'], unicode):
                json_res['token'] = json_res['token'].decode('UTF-8')
        return json_res['token']

    def test_wrong_bind(self):
        #/api/1.0/authenticate
        res = self.testapp.post('/user/bind', dumps({'uid': 'fake', 'password':'fake'}), status=401)

    def test_bind(self):
        #/api/1.0/authenticate
        res = self.testapp.post('/user/bind', dumps({'uid': self.user['id'], 'password':'fake'}), status=200)
        json_res = res.json
        self.assertTrue('token' in json_res)
        self.assertTrue('user' in json_res)
        self.assertTrue(self.user['id'] == json_res['user']['id'])
        # Return ASCII representation, not unicode
        return json_res['token'].encode('ascii','ignore')

    def test_wrong_token(self):
        token = self.test_authenticate()
        user = self.testapp.get('/api/'+self.api+'/user/'+str(self.user['id']), headers= {'Authorization':'Bearer '+'abcd'}, status=401)
        return user

    def test_renew_apikey(self):
        token = self.test_authenticate()
        res = self.testapp.delete('/api/'+self.api+'/user/'+str(self.user['id']+'/apikey'), headers= {'Authorization':'Bearer '+ token}, status=200)
        new_apikey = res.json
        self.assertTrue(new_apikey['apikey'] != self.user['credentials']['apikey'])
        updated_user = self.app.registry.db_mongo['users'].find_one({'id': self.user['id']})
        self.assertTrue(new_apikey['apikey'] == updated_user['credentials']['apikey'])
        user = self.testapp.get('/api/'+self.api+'/user/'+str(self.user['id']), headers= {'Authorization':'Bearer '+ token}, status=401)

    def test_revoke_apikey(self):
        token = self.test_authenticate()
        user = self.testapp.get('/api/'+self.api+'/user/'+str(self.user['id']), headers= {'Authorization':'Bearer '+ token}, status=200)
        self.assertTrue(user is not None)
        self.app.registry.db_mongo['users'].update({'id': self.user['id']},{'$set': {'credentials.apikey': '1234'}})
        user = self.testapp.get('/api/'+self.api+'/user/'+str(self.user['id']), headers= {'Authorization':'Bearer '+ token}, status=401)

    def test_user_authenticated(self):
        token = self.test_authenticate()
        user = self.testapp.get('/api/'+self.api+'/user/'+str(self.user['id']), headers= {'Authorization':'Bearer '+token}, status=200)
        return user

    def test_user_logged(self):
        self.testapp.get('/user/logged', status=404)
        token = self.test_authenticate()
        res = self.testapp.get('/user/logged', headers= {'Authorization':'Bearer '+token}, status=200)
        user = res.json
        self.assertTrue(user['id'] == self.user['id'])

    def test_user_edit(self):
        #/api/1.0/user
        token = self.test_authenticate()
        apikey = self.user['credentials']['apikey']
        self.user['credentials']['apikey'] = "abcd"
        res = self.testapp.put('/api/'+self.api+'/user/'+str(self.user['id']), dumps(self.user), headers= {'Authorization':'Bearer '+token}, status=200)
        json_res = res.json
        self.assertTrue(json_res['credentials']['apikey'] == "abcd")

    def test_project_create(self):
        token = self.test_authenticate()
        project = {
            'id': 'fake',
            'description': 'none',
            'members': [],
            'owner': 'test',
            'volumes': [],
            'prio': 50
        }
        res = self.testapp.post('/api/'+self.api+'/project', dumps(project), headers= {'Authorization':'Bearer '+token}, status=200)
        json_res = res.json
        self.assertTrue(json_res['id'] == project['id'])
        nb_projects = self.app.registry.db_mongo['projects'].find().count()
        self.assertTrue(nb_projects == 1)

    def test_project_edit(self):
        token = self.test_authenticate()
        project = {
            'id': 'fake',
            'description': 'none',
            'owner': 'test',
            'volumes': [],
            'members': [],
            'prio': 50
        }
        res = self.testapp.post('/api/'+self.api+'/project', dumps(project), headers= {'Authorization':'Bearer '+token}, status=200)
        project['prio'] = 10
        project['members'] = ['sample']
        project['quota_time'] = 0
        project['quota_cpu'] = 0
        project['quota_ram'] = 0
        res = self.testapp.post('/api/'+self.api+'/project/'+project['id'], dumps(project), headers= {'Authorization':'Bearer '+token}, status=200)
        json_res = res.json
        self.assertTrue(json_res['prio'] == project['prio'])
        db_project = self.app.registry.db_mongo['projects'].find_one({'id': project['id']})
        self.assertTrue(len(db_project['members']) == 1)

    def test_project_delete(self):
        token = self.test_authenticate()
        project = {
            'id': 'fake',
            'description': 'none',
            'members': [],
            'owner': 'test',
            'volumes': [],
            'prio': 50
        }
        res = self.testapp.post('/api/'+self.api+'/project', dumps(project), headers= {'Authorization':'Bearer '+token}, status=200)
        res = self.testapp.delete('/api/'+self.api+'/project/'+project['id'], headers= {'Authorization':'Bearer '+token}, status=200)
        nb_projects = self.app.registry.db_mongo['projects'].find().count()
        self.assertTrue(nb_projects == 0)

    def test_project_list(self):
        token = self.test_authenticate()
        project = {
            'id': 'fake',
            'description': 'none',
            'members': [],
            'owner': 'test',
            'volumes': [],
            'prio': 50
        }
        res = self.testapp.post('/api/'+self.api+'/project', dumps(project), headers= {'Authorization':'Bearer '+token}, status=200)
        res = self.testapp.get('/api/'+self.api+'/project', headers= {'Authorization':'Bearer '+token}, status=200)
        json_res = res.json
        self.assertTrue(len(json_res) == 1)

    def test_project_user_list(self):
        token = self.test_authenticate()
        project = {
            'id': 'fake',
            'description': 'none',
            'owner': 'test',
            'volumes': [],
            'members': ['sample'],
            'prio': 50
        }
        res = self.testapp.post('/api/'+self.api+'/project', dumps(project), headers= {'Authorization':'Bearer '+token}, status=200)
        res = self.testapp.get('/api/'+self.api+'/user/sample/project', headers= {'Authorization':'Bearer '+token}, status=200)
        json_res = res.json
        # fake + default
        self.assertTrue(len(json_res) == 2)

    def test_project_user_list_group_not_allowed(self):
        token = self.test_authenticate()
        project = {
            'id': 'fake',
            'description': 'none',
            'owner': 'test',
            'volumes': [],
            'members': [],
            'prio': 50
        }
        res = self.testapp.post('/api/'+self.api+'/project', dumps(project), headers= {'Authorization':'Bearer '+token}, status=200)
        res = self.testapp.get('/api/'+self.api+'/user/sample/project', headers= {'Authorization':'Bearer '+token}, status=200)
        json_res = res.json
        # fake + default
        self.assertTrue(len(json_res) == 1)

    def test_project_user_list_group_allowed(self):
        token = self.test_authenticate()
        project = {
            'id': 'fake',
            'description': 'none',
            'owner': 'test',
            'volumes': [],
            'members': [str(self.user['gid'])],
            'prio': 50
        }
        res = self.testapp.post('/api/'+self.api+'/project', dumps(project), headers= {'Authorization':'Bearer '+token}, status=200)
        res = self.testapp.get('/api/'+self.api+'/user/sample/project', headers= {'Authorization':'Bearer '+token}, status=200)
        json_res = res.json
        # fake + default
        self.assertTrue(len(json_res) == 2)


    def test_list_task_active(self):
        token = self.test_authenticate()
        tasks = self.testapp.get('/api/'+self.api+'/task/active', headers= {'Authorization':'Bearer '+token}, status=200)
        json_res = tasks.json
        self.assertTrue(len(json_res)>=0)
        return json_res

    def test_list_task_active_all(self):
        token = self.test_authenticate()
        tasks = self.testapp.get('/api/'+self.api+'/task/active/all', headers= {'Authorization':'Bearer '+token}, status=200)
        json_res = tasks.json
        self.assertTrue(len(json_res)>=0)
        return json_res

    def test_list_task_active_all_non_admin(self):
        self.user['id'] = 'www-fake'
        del self.user['_id']
        self.app.registry.db_mongo['users'].insert(self.user)
        token = self.test_authenticate()
        tasks = self.testapp.get('/api/'+self.api+'/task/active/all', headers= {'Authorization':'Bearer '+token}, status=403)

    def test_list_task_over(self):
        token = self.test_authenticate()
        tasks = self.testapp.get('/api/'+self.api+'/task/over', headers= {'Authorization':'Bearer '+token}, status=200)
        json_res = tasks.json
        self.assertTrue(len(json_res)>=0)
        return json_res

    def test_list_task_over_different_user(self):
        self.user['id'] = 'www-fake'
        del self.user['_id']
        self.app.registry.db_mongo['users'].insert(self.user)
        token = self.test_authenticate()
        tasks = self.testapp.get('/api/'+self.api+'/task/over', headers= {'Authorization':'Bearer '+token}, status=200)
        json_res = tasks.json
        self.assertTrue(len(json_res)==0)

    def test_list_task_over_all_non_admin(self):
        self.user['id'] = 'www-fake'
        del self.user['_id']
        self.app.registry.db_mongo['users'].insert(self.user)
        token = self.test_authenticate()
        tasks = self.testapp.get('/api/'+self.api+'/task/over/all', headers= {'Authorization':'Bearer '+token}, status=403)

    def test_list_task_over_cursor(self):
        token = self.test_authenticate()
        tasks = self.testapp.post('/api/'+self.api+'/task/over/0/100', dumps({}), headers= {'Authorization':'Bearer '+token}, status=200)
        json_res = tasks.json
        self.assertTrue(len(json_res)>=0)
        tasks = self.testapp.post('/api/'+self.api+'/task/over/100/100', dumps({}), headers= {'Authorization':'Bearer '+token}, status=200)
        json_res = tasks.json
        self.assertTrue(len(json_res)>=0)
        tasks = self.testapp.post('/api/'+self.api+'/task/over/0/100', dumps({'start': 10, 'end': 20}), headers= {'Authorization':'Bearer '+token}, status=200)
        json_res = tasks.json
        self.assertTrue(len(json_res)>=0)
        tasks = self.testapp.post('/api/'+self.api+'/task/over/0/100', dumps({'start': 10}), headers= {'Authorization':'Bearer '+token}, status=200)
        json_res = tasks.json
        self.assertTrue(len(json_res)>=0)
        tasks = self.testapp.post('/api/'+self.api+'/task/over/0/100', dumps({'end': 20}), headers= {'Authorization':'Bearer '+token}, status=200)
        json_res = tasks.json
        self.assertTrue(len(json_res)>=0)

    def test_count_tasks(self):
        res = self.testapp.get('/api/'+self.api+'/task/status/running/count',status=200)
        res = self.testapp.get('/api/'+self.api+'/task/status/all/count',status=200)
        res = self.testapp.get('/api/'+self.api+'/task/status/kill/count',status=200)

    def test_task_add(self):
        token = self.test_authenticate()
        res = self.testapp.post('/api/'+self.api+'/task', dumps(self.task), headers= {'Authorization':'Bearer '+token}, status=200)
        json_res = res.json
        self.assertTrue(json_res['id']>=0)
        running_task = self.app.registry.db_mongo['jobs'].find_one({'id': json_res['id']})
        self.assertTrue(running_task['status']['primary'] == 'pending')
        return json_res['id']

    def test_task_add_as_other_user(self):
        token = self.test_authenticate()
        self.task['user'] = {'id': self.other_user['id']}
        res = self.testapp.post('/api/'+self.api+'/task', dumps(self.task), headers= {'Authorization':'Bearer '+token}, status=200)
        json_res = res.json
        self.assertTrue(json_res['id']>=0)
        running_task = self.app.registry.db_mongo['jobs'].find_one({'id': json_res['id']})
        self.assertTrue(running_task['status']['primary'] == 'pending')
        self.assertTrue(running_task['user']['id'] == self.other_user['id'])
        return json_res['id']

    def test_task_add_as_other_user_not_allowed(self):
        token = self.test_authenticate(self.other_user)
        self.task['user'] = {'id': self.user['id']}
        res = self.testapp.post('/api/'+self.api+'/task', dumps(self.task), headers= {'Authorization':'Bearer '+token}, status=200)
        json_res = res.json
        self.assertTrue(json_res['id']>=0)
        running_task = self.app.registry.db_mongo['jobs'].find_one({'id': json_res['id']})
        self.assertTrue(running_task['status']['primary'] == 'pending')
        self.assertTrue(running_task['user']['id'] == self.other_user['id'])
        return json_res['id']

    def test_view_project_member_tasks_not_allowed(self):
        token = self.test_authenticate()
        project = {
            'id': 'fake',
            'description': 'none',
            'members': ['sample', 'invite'],
            'owner': 'test',
            'volumes': [],
            'prio': 50
        }
        self.task['user']['project'] = 'fake'
        res = self.testapp.post('/api/'+self.api+'/project', dumps(project), headers= {'Authorization':'Bearer '+token}, status=200)
        # Create first task
        res = self.testapp.post('/api/'+self.api+'/task', dumps(self.task), headers= {'Authorization':'Bearer '+token}, status=200)
        token_other_user = self.test_authenticate(as_user=self.other_user)
        res = self.testapp.post('/api/'+self.api+'/task', dumps(self.task), headers= {'Authorization':'Bearer '+token_other_user}, status=200)
        tasks = self.testapp.get('/api/'+self.api+'/task/active?project=fake', headers= {'Authorization':'Bearer '+token}, status=200)
        json_res = tasks.json
        self.assertTrue(len(json_res)==1)

    def test_view_project_member_tasks(self):
        token = self.test_authenticate()
        project = {
            'id': 'fake',
            'description': 'none',
            'members': ['sample', 'invite'],
            'owner': 'test',
            'volumes': [],
            'prio': 50
        }
        self.app.registry.god_config['share_project_tasks'] = True
        self.task['user']['project'] = 'fake'
        res = self.testapp.post('/api/'+self.api+'/project', dumps(project), headers= {'Authorization':'Bearer '+token}, status=200)
        # Create first task
        res = self.testapp.post('/api/'+self.api+'/task', dumps(self.task), headers= {'Authorization':'Bearer '+token}, status=200)
        token_other_user = self.test_authenticate(as_user=self.other_user)
        res = self.testapp.post('/api/'+self.api+'/task', dumps(self.task), headers= {'Authorization':'Bearer '+token_other_user}, status=200)
        tasks = self.testapp.get('/api/'+self.api+'/task/active?project=fake', headers= {'Authorization':'Bearer '+token}, status=200)
        json_res = tasks.json
        self.assertTrue(len(json_res)==2)

    def test_api_task_token(self):
        token = self.test_authenticate()
        task_id = self.test_task_add()
        res = self.testapp.get('/api/'+self.api+'/task/'+str(task_id)+'/token', headers= {'Authorization':'Bearer '+token}, status=200)
        json_res = res.json
        token = json_res['token']
        secret = self.app.registry.god_config['secret_passphrase']
        decoded_msg = jwt.decode(token, secret, audience='urn:godocker/api')
        self.assertTrue(decoded_msg['task']['id'] == task_id)

    def test_task_add_wrong_task(self):
        token = self.test_authenticate()
        del self.task['container']
        self.testapp.post('/api/'+self.api+'/task', dumps(self.task), headers= {'Authorization':'Bearer '+token}, status=400)

    def test_get_task_notfound(self):
        token = self.test_authenticate()
        res = self.testapp.get('/api/'+self.api+'/task/123', headers= {'Authorization':'Bearer '+token}, status=404)

    def test_get_task(self):
        token = self.test_authenticate()
        task_id = self.test_task_add()
        res = self.testapp.get('/api/'+self.api+'/task/'+str(task_id), headers= {'Authorization':'Bearer '+token}, status=200)
        json_res = res.json
        self.assertTrue(json_res['id'] == task_id)

    def test_get_task_status(self):
        token = self.test_authenticate()
        task_id = self.test_task_add()
        res = self.testapp.get('/api/'+self.api+'/task/'+str(task_id)+'/status', headers= {'Authorization':'Bearer '+token}, status=200)
        json_res = res.json
        self.assertTrue(json_res['id'] == task_id)
        self.assertTrue('status' in json_res)
        self.assertTrue('container' not in json_res)

    def test_kill_task_notfound(self):
        token = self.test_authenticate()
        res = self.testapp.delete('/api/'+self.api+'/task/123', headers= {'Authorization':'Bearer '+token}, status=404)

    def test_kill_task(self):
        token = self.test_authenticate()
        task_id = self.test_task_add()
        res = self.testapp.delete('/api/'+self.api+'/task/'+str(task_id), headers= {'Authorization':'Bearer '+token}, status=200)
        json_res = res.json
        self.assertTrue(json_res['id'] == task_id)
        killing_task = self.app.registry.db_mongo['jobs'].find_one({'id': json_res['id']})
        self.assertTrue(killing_task['status']['secondary'] == 'kill requested')
        res = self.testapp.get('/api/'+self.api+'/task/status/kill/count',status=200)
        json_res = res.json
        self.assertTrue(json_res['total'] == 1)

    def test_kill_all(self):
        token = self.test_authenticate()
        task_id = self.test_task_add()
        task_id = self.test_task_add()
        res = self.testapp.delete('/api/'+self.api+'/user/'+self.user['id']+'/task', headers= {'Authorization':'Bearer '+token}, status=200)
        json_res = res.json
        self.assertTrue(len(json_res) == 2)
        res = self.testapp.get('/api/'+self.api+'/task/status/kill/count',status=200)
        json_res = res.json
        self.assertTrue(json_res['total'] == 2)

    def test_kill_with_tag(self):
        token = self.test_authenticate()
        task_id = self.test_task_add()
        self.task['meta']['tags'] = ['test']
        task_id = self.test_task_add()
        res = self.testapp.delete('/api/'+self.api+'/user/'+self.user['id']+'/task?tag=test', headers= {'Authorization':'Bearer '+token}, status=200)
        json_res = res.json
        self.assertTrue(len(json_res) == 1)
        res = self.testapp.get('/api/'+self.api+'/task/status/kill/count',status=200)
        json_res = res.json
        self.assertTrue(json_res['total'] == 1)


    def test_switch_task_to_pending(self):
        token = self.test_authenticate()
        self.task['status'] = {}
        self.task['status']['primary'] = godutils.STATUS_CREATED
        res = self.testapp.post('/api/'+self.api+'/task', dumps(self.task), headers= {'Authorization':'Bearer '+token}, status=200)
        json_res = res.json
        created_task = self.app.registry.db_mongo['jobs'].find_one({'id': json_res['id']})
        self.assertTrue(created_task['status']['primary'] == godutils.STATUS_CREATED)
        res = self.testapp.get('/api/'+self.api+'/task/' + str(json_res['id'])+ '/pending',headers= {'Authorization':'Bearer '+token}, status=200)
        pending_task = self.app.registry.db_mongo['jobs'].find_one({'id': json_res['id']})
        self.assertTrue(pending_task['status']['primary'] == godutils.STATUS_PENDING)


    def test_user_rate_limit(self):
        self.app.registry.god_config['rate_limit'] = 1
        token = self.test_authenticate()
        res = self.testapp.post('/api/'+self.api+'/task', dumps(self.task), headers= {'Authorization':'Bearer '+token}, status=200)
        json_res = res.json
        self.assertTrue(json_res['id']>=0)
        running_task = self.app.registry.db_mongo['jobs'].find_one({'id': json_res['id']})
        self.assertTrue(running_task['status']['primary'] == 'pending')
        res = self.testapp.post('/api/'+self.api+'/task', dumps(self.task), headers= {'Authorization':'Bearer '+token}, status=403)

    def test_global_rate_limit(self):
        self.app.registry.god_config['rate_limit_all'] = 1
        token = self.test_authenticate()
        res = self.testapp.post('/api/'+self.api+'/task', dumps(self.task), headers= {'Authorization':'Bearer '+token}, status=200)
        json_res = res.json
        self.assertTrue(json_res['id']>=0)
        running_task = self.app.registry.db_mongo['jobs'].find_one({'id': json_res['id']})
        self.assertTrue(running_task['status']['primary'] == 'pending')
        res = self.testapp.post('/api/'+self.api+'/task', dumps(self.task), headers= {'Authorization':'Bearer '+token}, status=403)
